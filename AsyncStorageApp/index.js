import { AppRegistry } from 'react-native';
import App from './src/App';

AppRegistry.registerComponent('AsyncStorageApp', () => App);
