import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import First from './Containers/First';
import Second from './Containers/Second';
import Third from './Containers/Third';

const Screens = StackNavigator({
  First: { screen: First },
  Second: { screen: Second },
  Third: { screen: Third },
  initialRoute: 'First',
});


class App extends Component {
  render() {
    return (
      <Screens />
    );
  }
}


export default App;
