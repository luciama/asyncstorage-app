const Styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  inputStyle: {
    marginTop: 8,
    marginBottom: 8,
    marginRight: 8,
    marginLeft: 8,
    fontSize: 18,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    paddingLeft: 10,
  },
  buttonStyle: {
    padding: 10,
    alignItems: 'center',
    backgroundColor: '#fff',
    borderColor: '#007aff',
    borderRadius: 5,
    borderWidth: 1,
    marginTop: 8,
    marginBottom: 8,
    marginRight: 8,
    marginLeft: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
  },
};

export default Styles;
