import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Styles from './Styles';

class Second extends Component {
  static navigationOptions = {
    title: 'Screen 2',
    headerLeft: null,
  }

  componentDidMount() {}

  render() {
    return (
      <TouchableOpacity
        style={Styles.buttonStyle}
        onPress={() => this.props.navigation.navigate('Third')}
      >
        <Text>To Screen 3</Text>
      </TouchableOpacity>
    );
  }
}

export default Second;
