import React, { Component } from 'react';

import { NavigationActions } from 'react-navigation';
import { Text, TextInput, View, TouchableOpacity, AsyncStorage } from 'react-native';
import Styles from './Styles';

class First extends Component {
    static navigationOptions = {
      title: 'Login',
    }

    state = { email: '', password: '' }

    componentDidMount() {
      this.fetchData();
    }

    setEmail = (text) => {
      this.setState({ email: text });
      console.log(text);
    }

    setPassword = (text) => {
      this.setState({ password: text });
    }

    saveData = async () => {
      try {
        await AsyncStorage.setItem('email', this.state.email);
        this.props.navigation.navigate('Second');
      } catch (error) {
        alert(error);
      }
    }

    fetchData = async () => {
      try {
        const user = await AsyncStorage.getItem('email');


        if (user) {
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Second' })],
          });
          return this.props.navigation.dispatch(resetAction);
        }

        console.log('data is', user);
      } catch (error) {
        alert(error);
      }
    }

    render() {
      const { containerStyle, inputStyle, buttonStyle } = Styles;

      return (
        <View style={containerStyle}>
          <TextInput
            style={inputStyle}
            placeholder="Email"
            autoCapitalize="none"
            value={this.state.email}
            onChangeText={this.setEmail}
          />

          <TextInput
            secureTextEntry
            style={inputStyle}
            placeholder="Password"
            autoCapitalize="none"
            value={this.state.password}
            onChangeText={this.setPassword}
          />
          <TouchableOpacity
            style={buttonStyle}
            onPress={this.saveData}
          >
            <Text>Save</Text>
          </TouchableOpacity>
        </View>
      );
    }
}

export default First;
